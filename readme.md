Runner - Oriol Porta

Descripció:

Una porta intentan sobrevivint el maxim temps posible, y ha un vent que l'empenta cap a l'esquerra, a més d'empentar obstacles que si els toques caus y perds el control per 1 segon.

Risketos bàsics:

[x] Controls simples, ja sigui clicar un o dos botons.
[x] Moviment amb collisions.
[x] Aleatorietat.
[x] Creació d’objectes per Blueprint, ja sigui d’inici o un spawner (Relacionat amb el 1r Risketo Opcional).
[x] Puntuació mostrada per HUD, no val debug.

Risketos opcionals:

[] Fer una pool d’objectes.
[x] Augment progressiu de dificultat.

[] Mort i reinici.
[] Ús de parallax o altre format avançat de background.

Controls:
A: Moure's esquerra
D: Moure's dreta
Space: Saltar (Hi ha doble salt per el que pots saltar a l'aire una segona vegada)
